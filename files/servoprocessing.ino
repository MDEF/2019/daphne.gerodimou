#include <Servo.h>

const int servoPin=9;
Servo myservo;



void setup() {
  Serial.begin(9600);
  myservo.attach(servoPin);
}

void loop() {
 if (Serial.available() >0) {
   char val = Serial.read();

   int servoVal= map(val, 0, 255, 0, 180);
   myservo.write(servoVal);
   
  
  
  }
  delay(10);
}
