
int led = 3;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by


void setup() {
 
  pinMode(3, OUTPUT);
}


void loop() {

  analogWrite(3, brightness);

  
  brightness = brightness + fadeAmount;


  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }

  delay(30);
}
