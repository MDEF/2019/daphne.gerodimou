#define trigPin 7
#define echoPin 8

void setup() {
  Serial.begin (9600);
  pinMode(7, OUTPUT);
  pinMode(8, INPUT);
}

void loop() {
  float duration, distance;
  digitalWrite(7, LOW); 
  delayMicroseconds(2);
 
  digitalWrite(7, HIGH);
  delayMicroseconds(10);
  digitalWrite(7, LOW);
  
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) * 0.0344;
  
  if (distance >= 400 || distance <= 2){
    Serial.print("Distance = ");
    Serial.println("Out of range");
  }
  else {
    Serial.print("Distance = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500);
  }
  delay(500);
}
