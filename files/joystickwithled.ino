


const int LED1 = 10;
const int LED2 = 11;
const int LED3 = 12;
int xValue = 0 ; // read value of the X axis  
int yValue = 0 ; // read value of the Y axis  
int bValue = 0 ; // value of the button reading 





void setup() {
   pinMode(LED1, OUTPUT);
   pinMode(LED2, OUTPUT);
   pinMode(LED3, OUTPUT);
   pinMode(2,INPUT) ; // Configure Pin 2 as input
  digitalWrite(2,HIGH); 
   Serial.begin(9600); 

}

void loop() {


      // Read analog port values A0 and A1  
  xValue = analogRead(A0);  
  yValue = analogRead(A1);  

  // Read the logic value on pin 2  
  bValue = digitalRead(2);  

  // We display our data separated by a comma 
  Serial.print(xValue,DEC);
  Serial.print(",");
  Serial.print(yValue,DEC);
  Serial.print(",");
  Serial.print(!bValue);

  // We end with a newline character to facilitate subsequent analysis  
  Serial.print("\n");

  // Small delay before the next measurement  
  delay(10);  
  
  if(Serial.available()){  //id data is available to read

    char val = Serial.read();

    if(val == 'r'){       //if r received
      digitalWrite(11, HIGH); //turn on red led
      }
    if(val == 'b'){       //if b received
      digitalWrite(10, HIGH); //turn on blue led
      }
    if(val == 'y'){       //if y received
      digitalWrite(12, HIGH); //turn on yellow led
      }
    if(val == 'f'){       //if f received
      digitalWrite(11, LOW); //turn off all led
      digitalWrite(12, LOW);
      digitalWrite(10, LOW);
      }      
    }
  }
