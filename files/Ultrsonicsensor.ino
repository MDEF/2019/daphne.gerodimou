int trigPin = 7;  //Define the pins that you will work with
int echoPin = 8;
int LEDR = 10;
int LEDV = 11;
float Speed = 0.0343;  // Sound speed at cm/us
long duration, distance;

void setup()
{
   pinMode(7, OUTPUT);  //Define digital pin 7 as an output

   pinMode(8, INPUT);   //Define digital pin 8 as an input

   pinMode(10, OUTPUT);   //Define digital pin 10 as an output

   pinMode(11, OUTPUT);   //Define digital pin 11 as an output

   digitalWrite (10 , LOW);  // Define digital pin 10 in a low status

   digitalWrite (11 , LOW);  //Define digital pin 11 in a low status
}
void loop()
 {   
   digitalWrite(7, LOW);        // Make sure that the TRIG is deactivated

   delayMicroseconds(2);              // Make sure that the TRIG is in LOW

   digitalWrite(7, HIGH);       // Activate the output pulse 
   delayMicroseconds(10);             // Wait for 10µs, the pulse remains active during this time

   digitalWrite(7, LOW);        //Stop the pulse and wait for ECHO 

   duration = pulseIn(echoPin, HIGH); // pulseIn measures the time since the defined pin (echoPin) changes its status from low to high (from 0 to 1)

   distance = Speed* duration / 2;   //Divide by 2 because we want to have only the “go” time, not the “go and back” time
                                       // and divide by 29,1 because 1 is divided by the sound speed (1/SpeedSound) at cm/us

   if ( distance < 20){
       digitalWrite (10 , HIGH);     //If the sensor detects a distances less than 20 cm the red LED turns on

       digitalWrite (11 , LOW);      //and turns off the green LED

   }
   else{       // otherwise
       digitalWrite (10 , LOW);    // turn off the red LED
       digitalWrite (11 , HIGH);   //turn on the green LED 
   }
  

     
  
 } 
